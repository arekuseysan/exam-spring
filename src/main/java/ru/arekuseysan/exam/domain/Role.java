package ru.arekuseysan.exam.domain;

public enum Role
{
	CLIENT,
	MANAGER,
	ADMIN,
	TEST
}
