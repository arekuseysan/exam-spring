package ru.arekuseysan.exam.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "dealers")
public class Dealer
{
	private static final String SEQ_NAME = "dealer_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_NAME)
	@SequenceGenerator(name = SEQ_NAME, sequenceName = SEQ_NAME, allocationSize = 1)
	private Long id;

	private String name;
	private String licenseNumber;
	private String phone;
	private String ogrn;

	@OneToMany(cascade = CascadeType.ALL)
	public List<User> users;
}
