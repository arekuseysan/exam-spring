package ru.arekuseysan.exam.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User
{
	private static final String SEQ_NAME = "user_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_NAME)
	@SequenceGenerator(name = SEQ_NAME, sequenceName = SEQ_NAME, allocationSize = 1)
	private Long id;

	private String lastname;
	private String name;
	private String middleName;
	private String phone;
	private String inn;

	private String login;
	private String email;
	private String password;
	private boolean archive;
	@Enumerated(EnumType.STRING)
	private Role role;
	private short sex;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Car> cars;
}
