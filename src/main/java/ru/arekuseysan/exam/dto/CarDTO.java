package ru.arekuseysan.exam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.arekuseysan.exam.domain.User;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDTO
{
	private String brand;
	private String licensePlate;
	private String color;

	private List<User> owners;
}
