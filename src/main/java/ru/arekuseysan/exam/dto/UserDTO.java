package ru.arekuseysan.exam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.arekuseysan.exam.domain.Car;
import ru.arekuseysan.exam.domain.Role;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO
{
	private String lastname;
	private String name;
	private String middleName;
	private String phone;
	private String inn;

	private String login;
	private String email;
	private String password;
	private boolean archive;
	private Role role;
	private short sex;
	private List<Car> cars;
}
