package ru.arekuseysan.exam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.arekuseysan.exam.domain.User;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DealerDTO
{
	private String name;
	private String licenseNumber;
	private String phone;
	private String ogrn;

	private List<User> users;
}
