package ru.arekuseysan.exam.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.arekuseysan.exam.domain.Dealer;
import ru.arekuseysan.exam.dto.DealerDTO;
import ru.arekuseysan.exam.services.DealerService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/dealers", produces = "application/text")
public class DealerRestController
{
	private final DealerService dealerService;

	@Autowired
	public DealerRestController(DealerService dealerService)
	{
		this.dealerService = dealerService;
	}

	@Operation(
			summary = "Getting all dealers",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Getting dealers",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Dealer.class))
					)
			},
			tags = "GET"
	)
	@GetMapping({"", "/"})
	@ResponseBody
	public ResponseEntity<List<Dealer>> getAllDealers()
	{
		List<Dealer> dealerList = dealerService.getAll();

		return new ResponseEntity<>(dealerList, HttpStatus.OK);
	}

	@Operation(
			summary = "Getting dealer by id",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Getting dealer",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Dealer.class))
					),
					@ApiResponse(
							responseCode = "404",
							description = "Dealer not found"
					)
	},
			tags = "GET"
	)
	@GetMapping("/{id}")
	public ResponseEntity<Dealer> getDealerById(@PathVariable Long id, Model model)
	{
		Dealer dealer = dealerService.findDealerById(id);

		if (dealer == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(dealer, HttpStatus.OK);
	}
}
