package ru.arekuseysan.exam.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.arekuseysan.exam.domain.Car;
import ru.arekuseysan.exam.domain.Dealer;
import ru.arekuseysan.exam.services.CarService;
import ru.arekuseysan.exam.services.DealerService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cars")
public class CarRestController
{
	private final CarService carService;

	@Autowired
	public CarRestController(CarService carService)
	{
		this.carService = carService;
	}

	@Operation(
			summary = "Getting all dealers",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Getting dealers",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Dealer.class))
					)
			},
			tags = "GET"
	)
	@GetMapping({"", "/"})
	@ResponseBody
	public ResponseEntity<List<Car>> getAllDealers()
	{
		List<Car> carList = carService.getAll();

		return new ResponseEntity<>(carList, HttpStatus.OK);
	}

	@Operation(
			summary = "Getting dealer by id",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Getting dealer",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Dealer.class))
					),
					@ApiResponse(
							responseCode = "404",
							description = "Dealer not found"
					)
			},
			tags = "GET"
	)
	@GetMapping("/{id}")
	public ResponseEntity<Car> getDealerById(@PathVariable Long id, Model model)
	{
		Car car = carService.findCarById(id);

		if (car == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(car, HttpStatus.OK);
	}
}
