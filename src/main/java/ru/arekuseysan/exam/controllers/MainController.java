package ru.arekuseysan.exam.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.arekuseysan.exam.dao.CarRepository;
import ru.arekuseysan.exam.dao.DealerRepository;
import ru.arekuseysan.exam.dao.UserRepository;
import ru.arekuseysan.exam.domain.Car;
import ru.arekuseysan.exam.domain.Role;

import java.util.List;

@Controller
public class MainController
{
	private final UserRepository userRepo;
	private final CarRepository carRepo;
	private final DealerRepository dealerRepo;

	public MainController(CarRepository carRepo, UserRepository userRepo, DealerRepository dealerRepo)
	{
		this.carRepo = carRepo;
		this.userRepo = userRepo;
		this.dealerRepo = dealerRepo;
	}

	@RequestMapping({"", "/"})
	public String home(Model model, Authentication auth)
	{
		List<Car> cars = carRepo.findAll();

		if (auth != null)
			model.addAttribute("admin", auth.getAuthorities().contains(new SimpleGrantedAuthority(Role.ADMIN.name())));
		else
			model.addAttribute("admin", false);

		model.addAttribute("cars", cars);

		return "index";
	}

	@RequestMapping("/login")
	public String login()
	{
		return "login";
	}
}
