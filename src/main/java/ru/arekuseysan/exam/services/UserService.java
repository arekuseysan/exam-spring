package ru.arekuseysan.exam.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.arekuseysan.exam.domain.User;
import ru.arekuseysan.exam.dto.UserDTO;

import java.util.List;

public interface UserService extends UserDetailsService
{
	boolean save(UserDTO userDTO);

	boolean saveEdit(UserDTO userDTO, Long id);

	List<UserDTO> getAll();

	User findByUsername(String username);

	User findById(Long id);

	User findByLogin(String login);

	void demoteUserByUsername(String username);

	void demoteUserByLogin(String login);

	void demoteUserById(Long id);

	void promoteUserByUsername(String username);

	void promoteUserByLogin(String login);

	void promoteUserById(Long id);

	void deleteUserByUsername(String username);

	void deleteUserByLogin(String login);

	void deleteUserById(Long id);
}
