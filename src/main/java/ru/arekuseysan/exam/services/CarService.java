package ru.arekuseysan.exam.services;

import org.springframework.stereotype.Service;
import ru.arekuseysan.exam.dao.CarRepository;
import ru.arekuseysan.exam.domain.Car;

import java.util.List;
import java.util.Optional;

@Service
public class CarService
{
	private final CarRepository carRepo;

	public CarService(CarRepository dealerRepo)
	{
		this.carRepo = dealerRepo;
	}

	public List<Car> getAll()
	{
		return carRepo.findAll();
	}

	public Car findCarById(Long id)
	{
		Optional<Car> car = carRepo.findById(id);

		return car.orElse(null);
	}
}
