package ru.arekuseysan.exam.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.arekuseysan.exam.dao.UserRepository;
import ru.arekuseysan.exam.domain.User;
import ru.arekuseysan.exam.dto.UserDTO;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService
{
	private UserRepository userRepo;
	private PasswordEncoder encoder;

	public UserServiceImpl(UserRepository userRepo, PasswordEncoder encoder)
	{
		this.userRepo = userRepo;
		this.encoder = encoder;
	}

	@Override
	public boolean save(UserDTO userDTO)
	{
		return false;
	}

	@Override
	public boolean saveEdit(UserDTO userDTO, Long id)
	{
		return false;
	}

	@Override
	public List<UserDTO> getAll()
	{
		return userRepo.findAll().stream().map(this::toDTO).collect(Collectors.toList());
	}

	private UserDTO toDTO(User user)
	{
		return UserDTO.builder()
				.email(user.getEmail())
				.archive(user.isArchive())
				.inn(user.getInn())
				.lastname(user.getLastname())
				.middleName(user.getMiddleName())
				.login(user.getLogin())
				.name(user.getName())
				.phone(user.getPhone())
				.sex(user.getSex())
				.role(user.getRole())
				.cars(user.getCars())
				.build();
	}

	@Override
	public User findByUsername(String username)
	{
		return null;
	}

	@Override
	public User findById(Long id)
	{
		return null;
	}

	@Override
	public User findByLogin(String login)
	{
		return null;
	}

	@Override
	public void demoteUserByUsername(String username)
	{

	}

	@Override
	public void demoteUserByLogin(String login)
	{

	}

	@Override
	public void demoteUserById(Long id)
	{

	}

	@Override
	public void promoteUserByUsername(String username)
	{

	}

	@Override
	public void promoteUserByLogin(String login)
	{

	}

	@Override
	public void promoteUserById(Long id)
	{

	}

	@Override
	public void deleteUserByUsername(String username)
	{

	}

	@Override
	public void deleteUserByLogin(String login)
	{

	}

	@Override
	public void deleteUserById(Long id)
	{

	}

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException
	{
		return null;
	}
}
