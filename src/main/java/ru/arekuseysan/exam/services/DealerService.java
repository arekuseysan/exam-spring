package ru.arekuseysan.exam.services;

import org.springframework.stereotype.Service;
import ru.arekuseysan.exam.dao.DealerRepository;
import ru.arekuseysan.exam.domain.Dealer;
import ru.arekuseysan.exam.dto.DealerDTO;

import java.util.List;
import java.util.Optional;

@Service
public class DealerService
{
	private final DealerRepository dealerRepo;

	public DealerService(DealerRepository dealerRepo)
	{
		this.dealerRepo = dealerRepo;
	}

	public List<Dealer> getAll()
	{
		return dealerRepo.findAll();
	}

	public boolean save(DealerDTO dto)
	{
		Dealer dealer = Dealer.builder()
							  .licenseNumber(dto.getLicenseNumber())
							  .name(dto.getName())
							  .ogrn(dto.getOgrn())
							  .phone(dto.getPhone())
							  .build();
		dealerRepo.save(dealer);
		return true;
	}

	public Dealer findDealerById(Long id)
	{
		Optional<Dealer> dealer = dealerRepo.findById(id);

		return dealer.orElse(null);
	}

	public boolean saveEdit(Long id, DealerDTO dto)
	{
		return true;
	}
}
