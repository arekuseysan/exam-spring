package ru.arekuseysan.exam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.arekuseysan.exam.domain.Dealer;

public interface DealerRepository extends JpaRepository<Dealer, Long>
{
}
