package ru.arekuseysan.exam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.arekuseysan.exam.domain.Car;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Long>
{
	List<Car> findByOwnerId(Long ownerId);

	List<Car> findByOwnerIdAndDealerId(Long ownerId, Long dealerId);
}
