package ru.arekuseysan.exam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.arekuseysan.exam.domain.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>
{
	Optional<User> findUserByLogin(String login);

	Optional<User> findUserByName(String name);
}
